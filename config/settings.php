<?php

const BASE_URL = "http://localhost:8081/";

/****** DEBUG MODE ******/
const DEBUG =  true;

/****** SECRET FOR HASH ******/
const COOKIE_SALT_SECRET = "c172f16bcee5df8ee574e125430c6b0c14989109fac2fe992bfdae8e63c858c6";
const REGISTER_SALT_SECRET = "f6df8e0c143f4ef26bee979acc151052998ec58c6bce92140bf563c18e87dae2";

/****** USERS RANKS ******/
const GUEST = 0;
const CONNECTED = 1;
const MODERATOR = 2;
const ADMIN = 3;

/****** ROUTES DIRECTORIES INDEX ******/
const START_LAYOUT = 0;
const DEFAULT_LAYOUT = 1;
const ERROR_LAYOUT = 2;
const HELPERS = 3;

/****** ROUTES DIRECTORIES VALUES ******/
const CLASS_DIR = "class/";
const VIEWS_DIR = "views/";
const LAYOUTS_DIR = "layouts/";
const HELPERS_DIR = "helpers/";

?>