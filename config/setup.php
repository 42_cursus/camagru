<?php

require "app/class/App.class.php";

use App\Tools\App;
use App\Tools\Database;


echo '
<style>
body { 
    background: black;
    color: white;
}


</style>';


$app = App::getInstance();
echo 'Connection to Database<br>';
$db = Database::getInstance(true);

echo 'Remove captures in "public/img/captures"<br>';
foreach (scandir('./public/img/captures') as $filename)
    if (substr($filename, -strlen(".png")) == ".png")
        unlink("public/img/captures/$filename");

echo 'Run sql file init';
App::debugDumpVar($db->runSQLFile('config/sql/camagru.sql'));
echo 'DATABASE ' . DB_NAME . ' has been created';
