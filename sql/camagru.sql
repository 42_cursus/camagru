/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100135
 Source Host           : 127.0.0.1:3306
 Source Schema         : camagru

 Target Server Type    : MariaDB
 Target Server Version : 100135
 File Encoding         : 65001

 Date: 16/04/2019 15:08:40
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gallery_comment_template
-- ----------------------------
DROP TABLE IF EXISTS `gallery_comment_template`;
CREATE TABLE `gallery_comment_template`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_gallery` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `publish` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for gallery_like_template
-- ----------------------------
DROP TABLE IF EXISTS `gallery_like_template`;
CREATE TABLE `gallery_like_template`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_gallery` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for gallery_template
-- ----------------------------
DROP TABLE IF EXISTS `gallery_template`;
CREATE TABLE `gallery_template`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `publish` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gallery_template
-- ----------------------------
INSERT INTO `gallery_template` VALUES (21, 3, 'G1ZBTV6W0ewR', '2019-04-02 14:12:31');
INSERT INTO `gallery_template` VALUES (22, 3, 'ie1tP5AOBc6U', '2019-04-02 14:12:34');
INSERT INTO `gallery_template` VALUES (25, 3, 'Cc4EF9ZK0Yzh', '2019-04-02 14:13:24');
INSERT INTO `gallery_template` VALUES (26, 3, 'WjoNp9Y3UPT6', '2019-04-02 14:13:25');
INSERT INTO `gallery_template` VALUES (27, 3, 'hPM3fgvQW4FV', '2019-04-02 14:13:26');
INSERT INTO `gallery_template` VALUES (28, 3, 'Zg9Y0IOsdHi5', '2019-04-02 14:13:27');
INSERT INTO `gallery_template` VALUES (29, 3, 'mM8R7FLiAkQ3', '2019-04-02 14:13:28');
INSERT INTO `gallery_template` VALUES (30, 3, 'VC0fL7pZGrFU', '2019-04-02 14:13:30');
INSERT INTO `gallery_template` VALUES (31, 3, '4geEG6rxbA5o', '2019-04-02 14:13:31');
INSERT INTO `gallery_template` VALUES (32, 3, 'pLODzKlaWUFH', '2019-04-02 14:13:31');
INSERT INTO `gallery_template` VALUES (33, 3, 'Yzp3mlanWFLe', '2019-04-02 14:13:31');
INSERT INTO `gallery_template` VALUES (34, 3, 'kWAeZTixHS0G', '2019-04-02 14:13:31');
INSERT INTO `gallery_template` VALUES (35, 3, 'IWLuoABt2dck', '2019-04-02 14:13:32');
INSERT INTO `gallery_template` VALUES (36, 3, 'nxzBmjH92QoM', '2019-04-02 14:13:32');
INSERT INTO `gallery_template` VALUES (37, 3, 'tHdG8igS4Ib5', '2019-04-02 14:13:32');
INSERT INTO `gallery_template` VALUES (38, 3, 'bqMt0RwmIHu3', '2019-04-02 14:13:32');
INSERT INTO `gallery_template` VALUES (39, 3, 'kzDsviJEjpqQ', '2019-04-02 14:13:32');

-- ----------------------------
-- Table structure for users_template
-- ----------------------------
DROP TABLE IF EXISTS `users_template`;
CREATE TABLE `users_template`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `level` int(1) NOT NULL DEFAULT 1,
  `forgot` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users_template
-- ----------------------------
INSERT INTO `users_template` VALUES (3, 'adelhom', 'timed2a@hotmail.fr', '29ea6bde34df15f45096de4ee1a6d44bd26de44d552a8250116b882d210e54a92163e5ab26b5fe3ffa58823717e39a8ac183f3a754f405f0074f8d14d7654ce4', 1, NULL);

SET FOREIGN_KEY_CHECKS = 1;
