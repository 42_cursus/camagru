<?php

namespace App;
use App\Tools\App;
use App\Tools\Database;
use App\Tools\Router;
use App\Tools\User;

require_once "class/App.class.php";

$router = Router::getInstance();

/***********************************************************************************************************************
 *                                          ROUTES CONFIGURATION
 **********************************************************************************************************************/

$router->addRoute("/register",              "register",                     START_LAYOUT,           GUEST,       true);
$router->addRoute("/auth",                  "auth",                         START_LAYOUT,           GUEST,       true);
$router->addRoute("/forgot",                "forgot",                       START_LAYOUT,           GUEST,       true);
$router->addRoute("/activation",            "activation",                   START_LAYOUT,           GUEST,       true);
$router->addRoute("/reset",                 "reset",                        START_LAYOUT,           GUEST,       true);
$router->addRoute("/preview",               "preview",                      START_LAYOUT,           GUEST,       true);

$router->addRoute("/",                      "home",                         DEFAULT_LAYOUT,         CONNECTED,   false);
$router->addRoute("/gallery",               "gallery",                      DEFAULT_LAYOUT,         CONNECTED,   false);
$router->addRoute("/settings",              "settings",                     DEFAULT_LAYOUT,         CONNECTED,   false);
$router->addRoute("/comment",               "comment",                      DEFAULT_LAYOUT,         CONNECTED,   false);

$router->addRoute("/404",                   "404",                          ERROR_LAYOUT,           GUEST,       false);

$router->addRoute("/forms/register",        "/forms/register",              HELPERS,                GUEST,       true);
$router->addRoute("/forms/auth",            "/forms/auth",                  HELPERS,                GUEST,       true);
$router->addRoute("/forms/forgot",          "/forms/forgot",                HELPERS,                GUEST,       true);
$router->addRoute("/forms/comment",         "/forms/comment",               HELPERS,                CONNECTED,   false);
$router->addRoute("/forms/settings/global", "/forms/settings/global",       HELPERS,                CONNECTED,   false);
$router->addRoute("/forms/settings/password","/forms/settings/password",    HELPERS,                CONNECTED,   false);
$router->addRoute("/capture",               "/ajax/capture",                HELPERS,                CONNECTED,   false);
$router->addRoute("/like",                  "/ajax/like",                   HELPERS,                CONNECTED,   false);
$router->addRoute("/usergallery",           "/ajax/userGallery",            HELPERS,                CONNECTED,   false);
$router->addRoute("/deleteusergallery",     "/ajax/deleteUserGallery",      HELPERS,                CONNECTED,   false);
$router->addRoute("/allgallery",            "/ajax/allGallery",             HELPERS,                CONNECTED,   false);
$router->addRoute("/logout",                "/logout",                      HELPERS,                CONNECTED,   false);

session_start();

$route = $router->getCurrentRoute();
require_once $router->getPath();

?>