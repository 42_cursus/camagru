<?php

header("Content-Type: application/json");
\App\Tools\App::isXMLHttpRequest();
$data = json_decode(file_get_contents("php://input"), true);

$form = \App\Tools\Form::getInstance();
$form->setPosts($data);
$form->setExpected(["id"]);

if ($form->isValide()) {
    $id = $form->getOnePosts("id");
    if (empty($id))
        exit(json_encode("fail"));
    $gallery = \App\Tools\Gallery::getInstance()->getGallery($id);
    if ($gallery['id_user'] != $_SESSION['id'])
        exit(json_encode("fail"));
    \App\Tools\Gallery::getInstance()->deleteGallery($id);
    exit(json_encode("success"));
}
else
    header('HTTP/1.1 500 Internal Server Error');
?>