<?php

header("Content-Type: application/json");
\App\Tools\App::isXMLHttpRequest();

exit(json_encode(\App\Tools\Gallery::getInstance()->getUserGallery($_SESSION['id'])));

?>