<?php

header("Content-Type: application/json");
\App\Tools\App::isXMLHttpRequest();
$data = json_decode(file_get_contents("php://input"), true);

$form = \App\Tools\Form::getInstance();
$form->setPosts($data);
$form->setExpected(["min", "max"]);

if ($form->isValide()) {
    foreach ($form->getPosts() as $p)
        if (empty($p))
            exit(json_encode("fail"));
        else {
            $gallery = \App\Tools\Gallery::getInstance();
            $user = \App\Tools\User::getInstance();
            $datas = $gallery->getAllGalleryLimits($form->getOnePosts('min'), $form->getOnePosts('max'));
            foreach ($datas as $k => &$v)
                $v['username'] = $user->getOneById($v['id_user'])['username'];
            exit(json_encode($datas));
        }
}
else
    header('HTTP/1.1 500 Internal Server Error');
?>