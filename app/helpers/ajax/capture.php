<?php

header("Content-Type: application/json");
\App\Tools\App::isXMLHttpRequest();
$data = json_decode(file_get_contents("php://input"), true);

$form = \App\Tools\Form::getInstance();
$form->setPosts($data);
$form->setExpected(["source", "sourceIcon", "id", "x", "y", "s"]);

if ($form->isValide()) {
    foreach ($form->getPosts() as $p)
        if (empty($p))
            exit(json_encode("Invalide capture, try again please!"));
        else {
            $image = new \App\Tools\Image($form->getPosts());
            if ($image->create())
                exit(json_encode("success"));
            else 
                exit(json_encode("Invalide capture, try again please!"));
        }
}
else
    header('HTTP/1.1 500 Internal Server Error');
?>