<?php

use App\Tools\App;
use App\Tools\Form;
use App\Tools\User;

$app = App::getInstance();
$form = Form::getInstance();
$user = User::getInstance();

$form->setPosts($_POST);
$form->setExpected(["usernameOrEmail", "password", "submit"]);

if ($form->isValide()) {
    if (($error = $user->auth($form->getOnePosts("usernameOrEmail"), $form->getOnePosts("password"))) === true)
        $app->redirectSuccessMessage("You're log now !", "/");
    else
        $app->redirectErrorMessage($error, "/auth");
}
else {
    $app->redirectErrorMessage("Invalide form", "/auth");
}

?>