<?php

use App\Tools\App;
use App\Tools\Form;
use App\Tools\User;

$app = App::getInstance();
$form = Form::getInstance();
$user = User::getInstance();

$form->setPosts($_POST);
$form->setExpected(["current_password", "new_password"]);

if ($form->isValide()) {
    $current = $form->getOnePosts("current_password");
    $new = $form->getOnePosts("new_password");
    $hash = $user->getOneById($_SESSION['id'])["password"];
    if (empty($current))
        $app->redirectErrorMessage("Current password can't be empty", "/settings");
    if (empty($new))
        $app->redirectErrorMessage("New password can't be empty", "/settings");
    if (!hash_equals(App::hash($current . REGISTER_SALT_SECRET), $hash))
        $app->redirectErrorMessage("This is not your current password", "/settings");
    if ($current == $new)
        $app->redirectErrorMessage("This is the same password", "/settings");
    if(!in_array(strlen($new), range(4, 32))) //
        $app->redirectErrorMessage("Password must contain between 4 & 32 characters", "/settings");
    $oneUpper = (bool) preg_match('/[A-Z]/', $new);
    if (!$oneUpper)
        $app->redirectErrorMessage("Password must contain one upper character at least", "/settings");
    $user->setPassword($new);
    $app->redirectSuccessMessage("Password has been changed", "/settings");
}
else {
    $app->redirectErrorMessage("Invalide form", "/settings");
}

?>