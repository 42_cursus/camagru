<?php

use App\Tools\App;
use App\Tools\Form;
use App\Tools\User;

$app = App::getInstance();
$form = Form::getInstance();
$user = User::getInstance();

$form->setPosts($_POST);
$form->setExpected(["username", "email", "notify"]);

if ($form->isValide()) {
    $username = $form->getOnePosts("username");
    $email = $form->getOnePosts("email");

    $u = $user->getOneById($_SESSION['id']);

    $check_username = $user->valideUsername($username);
    $check_email = $user->valideEmail($email);

    if ($username === $u['username'])
        $check_username = true;
    if ($email === $u['email'])
        $check_email = true;

    if ($check_username !== true)
        $app->redirectErrorMessage($check_username, "/settings");
    if ($check_email !== true)
        $app->redirectErrorMessage($check_email, "/settings");

    $user->setUsername($username);
    $user->setEmail($email);
    $user->setNotify($form->getOnePosts('notify'));
    $app->redirectSuccessMessage("Account has been updated", "/settings");
}
else {
    $app->redirectErrorMessage("Invalide form", "/settings");
}

?>