<?php

use App\Tools\App;
use App\Tools\Form;
use App\Tools\User;

$app = App::getInstance();
$form = Form::getInstance();
$user = User::getInstance();

$form->setPosts($_POST);
$form->setExpected(["id", "content", "submit"]);

if ($form->isValide()) {
    foreach ($form->getPosts() as $p)
        if (empty($p))
            $app->redirectErrorMessage("Invalide comment!", "/comment/" . $form->getOnePosts('id'));
    \App\Tools\Gallery::getInstance()->setComment($form->getOnePosts('id'), $form->getOnePosts('content'));
    $app->redirectSuccessMessage("Comment send!", "/comment/" . $form->getOnePosts('id'));
}
else {
    $app->redirectErrorMessage("Invalide comment!", "/comment/" . $form->getOnePosts('id'));
}

?>