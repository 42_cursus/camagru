<?php

use App\Tools\App;
use App\Tools\Form;
use App\Tools\User;

$app = App::getInstance();
$form = Form::getInstance();
$user = User::getInstance();

$form->setPosts($_POST);
$form->setExpected(["email", "submit"]);

if ($form->isValide()) {
    if (($error = $user->sendForgot($form->getOnePosts("email"))) === true)
        $app->redirectSuccessMessage("Mail sent to " . $form->getOnePosts("email") . " follow instructions to reset password", "/auth");
    else
        $app->redirectErrorMessage($error, "/forgot");
}
else {
    $app->redirectErrorMessage("Invalide form", "/forgot");
}

?>