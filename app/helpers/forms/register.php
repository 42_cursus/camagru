<?php

use App\Tools\App;
use App\Tools\Form;
use App\Tools\User;

$app = App::getInstance();
$form = Form::getInstance();
$user = User::getInstance();

$form->setPosts($_POST);
$form->setExpected(["username", "email", "password1", "password2", "submit"]);

if ($form->isValide()) {
    if (($error = $user->canRegister($form->getPosts())) === true) {
        $user->register($form->getPosts());
        $app->redirectSuccessMessage("Nice ! A mail send to " . $form->getOnePosts("email"), "/");
    }
    else
        $app->redirectErrorMessage($error, "/register");
}
else {
    $app->redirectErrorMessage("Invalide form", "/register");
}

?>