<?php
use App\Tools\User;

$user = User::getInstance();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Welcome</title>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
    <link rel="stylesheet" type="text/css" href="/public/css/global.css"/>
    <link rel="stylesheet" type="text/css" href="/public/css/input.css"/>
    <link rel="stylesheet" type="text/css" href="/public/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="/public/css/default.css">
</head>
<body>
<div id="alertDatas">
<?php $user->alert(["success", "error"]); ?>
</div>

<header class="wrapper clearfix">
    <div id="banner">
        <div id="logo">
            <a href="/"><img src="/public/img/logo_min.png" alt="logo"></a>
        </div>
    </div>
    <nav id="topnav" role="navigation">
        <ul class="srt-menu" id="menu-main-navigation">
            <li <?php if ($route['route'] == '/') echo 'class="current"'?>><a href="/">Home</a></li>
            <li <?php if ($route['route'] == '/gallery') echo 'class="current"'?>><a href="/gallery">Gallery</a></li>
            <li <?php if ($route['route'] == '/settings') echo 'class="current"'?>><a href="/settings">Settings</a></li>
            <li><a href="/logout">Logout</a></li>
        </ul>
    </nav>
</header>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/app/views/" . $route['path']; ?>

<footer>
    <div id="colophon" class="wrapper clearfix">
        Camagru
    </div>
    <div id="attribution" class="wrapper clearfix" style="color:#666; font-size:11px;">
        Copyright adelhom - <?php echo date("Y"); ?> @42
    </div>
</footer>

<script src="/public/js/camagru.js"></script>
<script src="/public/js/tabs.js"></script>

</body>
</html>