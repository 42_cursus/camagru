<?php
    use App\Tools\User;

    $user = User::getInstance();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>Welcome</title>
    <link rel="stylesheet" type="text/css" href="/public/css/global.css" />
    <link rel="stylesheet" type="text/css" href="/public/css/input.css" />
    <link rel="stylesheet" type="text/css" href="/public/css/start.css" />
    <style>
        .previewButton {
            -moz-box-shadow: 0px 10px 14px -7px #276873;
            -webkit-box-shadow: 0px 10px 14px -7px #276873;
            box-shadow: 0px 10px 14px -7px #276873;
            background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #4c6569), color-stop(1, #1c6c7a));
            background:-moz-linear-gradient(top, #4c6569 5%, #1c6c7a 100%);
            background:-webkit-linear-gradient(top, #4c6569 5%, #1c6c7a 100%);
            background:-o-linear-gradient(top, #4c6569 5%, #1c6c7a 100%);
            background:-ms-linear-gradient(top, #4c6569 5%, #1c6c7a 100%);
            background:linear-gradient(to bottom, #4c6569 5%, #1c6c7a 100%);
            filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#4c6569', endColorstr='#1c6c7a',GradientType=0);
            background-color:#4c6569;
            -moz-border-radius:8px;
            -webkit-border-radius:8px;
            border-radius:8px;
            display:inline-block;
            cursor:pointer;
            color:#ffffff;
            font-family:"Arial Black";
            font-size:20px;
            font-weight:bold;
            padding:13px 32px;
            text-decoration:none;
            text-shadow:0px 1px 0px #3d768a;
        }
        .previewButton:hover {
            background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #1c6c7a), color-stop(1, #4c6569));
            background:-moz-linear-gradient(top, #1c6c7a 5%, #4c6569 100%);
            background:-webkit-linear-gradient(top, #1c6c7a 5%, #4c6569 100%);
            background:-o-linear-gradient(top, #1c6c7a 5%, #4c6569 100%);
            background:-ms-linear-gradient(top, #1c6c7a 5%, #4c6569 100%);
            background:linear-gradient(to bottom, #1c6c7a 5%, #4c6569 100%);
            filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#1c6c7a', endColorstr='#4c6569',GradientType=0);
            background-color:#1c6c7a;
        }
        .previewButton:active {
            position:relative;
            top:1px;
        }
    </style>
</head>
<body>
<?php $user->alert(["success", "error"]); ?>

<a href="/auth"><div id="logo"></div></a>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/app/views/" . $route['path']; ?>
</body>
</html>


