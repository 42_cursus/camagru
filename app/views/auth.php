<?php
use App\Tools\Form;
$form = Form::getInstance();

?>

<div align="center">
    <a href="/preview" class="previewButton">Gallery Preview</a>
</div>

<div class="form-start">
    <form method="POST" action="/forms/auth">
        <h1>Authentification</h1>

        <?php $form->groupInput("usernameOrEmail", "", "text", "Your username or email", "u", "Username or email", true); ?>
        <?php $form->groupInput("password", "", "password", "Your password", "p", "***********", true); ?>
        <?php $form->groupSubmit("submit", "Log in"); ?>

        <div class="form-group">
            Not a member yet ? <a href="/register"> Go and register </a>
        <div class="form-group">
            Forgot your password ? <a href="/forgot"> Give me a new password !</a>
        </div>
    </form>
</div>