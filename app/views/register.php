<?php
    use App\Tools\Form;
    $form = Form::getInstance();
?>

<div class="form-start">
    <form method="POST" action="/forms/register ">
        <h1>Register</h1>

        <?php $form->groupInput("username", "", "text", "Your username", "u", "mysuperusername123", true); ?>
        <?php $form->groupInput("email", "", "email", "Your email", "e", "mysupermail@mail.com", true); ?>
        <?php $form->groupInput("password1", "", "password", "Your password", "p", "ex : @ppzt35a!'", true); ?>
        <?php $form->groupInput("password2","", "password", "Your password confirmation", "p", "ex : @ppzt35a!'", true); ?>
        <?php $form->groupSubmit("submit", "Sign up"); ?>

        <div class="input-group">
            Already a member ? <a href="/auth"> Go and log in </a>
        </div>
    </form>
</div>

