<?php

$form = \App\Tools\Form::getInstance();
$gallery = \App\Tools\Gallery::getInstance();
$user = \App\Tools\User::getInstance();

?>
<link rel="stylesheet" type="text/css" href="/public/css/global.css"/>
<link rel="stylesheet" type="text/css" href="/public/css/input.css"/>
<link rel="stylesheet" type="text/css" href="/public/css/normalize.css">
<link rel="stylesheet" type="text/css" href="/public/css/default.css">
<style>
    html {
        overflow: -moz-scrollbars-vertical;
        overflow-y: scroll!important;
        min-height: 100%;
    }
</style>
<div id="main" class="wrapper clearfix">
    <section id="gallery">
        <?php
        $g = $gallery->getAllGallery();
        if (count($g) == 0)
            echo "No captures";
        else
            foreach ($gallery->getAllGalleryLimits(0, 6) as $k => $v) { ?>
                <div class="gallery-item-capture">
                    <div class="gallery">
                        <div class="desc" style="color: #5ec79e"><?= $user->getOneById($v['id_user'])['username']?></div>
                        <a target="_blank" href="/public/img/captures/<?= $v['name'] . ".png" ?>">
                            <img src="/public/img/captures/<?= $v['name'] . ".png" ?>" alt="" width="300" height="200">
                        </a>
                    </div>
                </div>
            <?php } ?>


    </section>
    <div align="center"><button id="loadmore">Load more</button></div>
</div>
<script src="/public/js/camagru.js"></script>
<script src="/public/js/infinite-preview.js"></script>
<script>
    var hasScrollbar = window.innerWidth > document.documentElement.clientWidth;
    if (hasScrollbar) {
        var lm = document.getElementById('loadmore');
        lm.onclick = function() {
            loadMore();
            hasScrollbar = window.innerWidth > document.documentElement.clientWidth;
            if (hasScrollbar)
                lm.style.visibility = 'hidden';
        };
    } else {
        lm.style.visibility = 'hidden';
    }
</script>