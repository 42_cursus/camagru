<?php

$form = \App\Tools\Form::getInstance();
$user = \App\Tools\User::getInstance();
$me = $user->getOneById($_SESSION['id']);

?>

<section id="page-header" class="clearfix">

    <div class="wrapper">
        <h1>Your account settings</h1>
    </div>
</section>


<div id="main" class="wrapper clearfix">
    <div class="container">
        <h2>Change global</h2>
        <form method="post" action="forms/settings/global">
            <label style="color: #5ec79e">Username</label><input name="username" type="text" value="<?php echo $me["username"] ?>">
            <label style="color: #5ec79e">Email</label><input name="email" type="text" value="<?php echo $me["email"] ?>">
            <label style="color: #5ec79e; margin-right: 10px">Notifications Enabled</label><input id="visible_notify" type="checkbox" <?php if ($me["notify"] == 1) echo "checked" ?>>
            <input id="notify" name="notify" type="hidden" value="<?= $me["notify"] ?>">
            <input class="button" type="submit" value="Edit">
        </form>
        <h2>Change password</h2>
        <form method="post" action="forms/settings/password">
            <label style="color: #5ec79e">Current password</label><input name="current_password" type="password">
            <label style="color: #5ec79e">New password</label><input name="new_password" type="password">
            <input class="button" type="submit" value="Edit">
        </form>
    </div>
</div>

<script>
    const checkbox = document.getElementById('visible_notify');
    const hidden = document.getElementById('notify');

    checkbox.addEventListener('change', (event) => {
        if (event.target.checked) {
            hidden.value = 1
        } else {
            hidden.value = 0
        }
    })

</script>
