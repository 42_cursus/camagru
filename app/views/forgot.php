<?php
use App\Tools\Form;
$form = Form::getInstance();
?>

<div class="form-start">
    <form method="POST" action="/forms/forgot ">
        <h1>Forgot my password</h1>
        <?php $form->groupInput("email", "", "email", "Your email", "e", "mysupermail@mail.com", true); ?>
        <?php $form->groupSubmit("submit", "Send"); ?>

    </form>
</div>

