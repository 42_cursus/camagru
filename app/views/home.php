<?php

$form = \App\Tools\Form::getInstance();
$gallery = \App\Tools\Gallery::getInstance();

?>

<section id="page-header" class="clearfix">

    <div class="wrapper">
        <h1>Capture yourself</h1>
    </div>
</section>


<div id="main" class="wrapper clearfix">
    <section id="content" style="display: none">
        <div id="builder">
            <div id="tabsOptions">
                <div id="tab-links">
                    <a href="#tab_1" class="tab-link">Icons</a>
                    <a href="#tab_2" class="tab-link">Position</a>
                </div>

                <div id="tab-contents">
                    <div id="tab_1" class="tab-content alive">
                        <?php $form->allIcons() ?>
                    </div>
                    <div id="tab_2" class="tab-content">
                        <?php $form->groupRange("iconX", "iconX", "Position X", [-400, 800], 1, 200); ?>
                        <?php $form->groupRange("iconY", "iconY", "Position Y", [-400, 800], 1, 200); ?>
                        <?php $form->groupRange("iconS", "iconS", "Size", [10, 100]); ?>
                    </div>
                </div>
            </div>
            <video style="display: none" width="400" height="400" id="webcam" autoplay></video>
            <img style="display: none" src="/public/img/blank.png" width="400" height="400" id="upload-render">
            <input id="upload" type="file" accept="image/*" style="display: none">
            <canvas width="400" height="400" id="icon"></canvas>
        </div>
        <?php $form->submit("capture", "Capture", true); ?>
    </section>


    <aside id="yours">
        <h2>Your captures</h2>
        <div id="usergallery">
            <?php
            $g = $gallery->getUserGallery($_SESSION['id']);
            if (count($g) == 0)
                echo "No captures";
            else
                foreach ($gallery->getUserGallery($_SESSION['id']) as $k => $v) {
                    ?>
                    <div class="gallery-item-capture">
                        <div class="gallery">
                            <a target="_blank" href="/public/img/captures/<?= $v['name'] . ".png" ?>">
                                <img src="/public/img/captures/<?= $v['name'] . ".png" ?>" alt="" width="300"
                                     height="200">
                            </a>
                            <div class="desc">
                                <?= $v['nb_like'] . " " ?><span style="display: inline-block" class="thumb"></span>
                                <?= $v['nb_comments'] . " " ?><span style="display: inline-block" class="chat"></span>
                                <span onclick="deleteUserGallery(this)" id="<?= $v['id'] ?>" class="delete-gallery"
                                      style="float: right">X</span>
                            </div>
                        </div>
                    </div>
                <?php } ?>
        </div>
    </aside>
</div>

<script src="/public/js/capture.js"></script>
<script language="javascript">
    window.onload = function () {
        new Tabs('.tab-link', {
            contentActive: 'alive',
            allowClose: true,
            activeTab: 0
        }).forEach(function (tabLink) {
            new Tabs(tabLink.hash + ' .tab-inner_link');
        });
    };

</script>