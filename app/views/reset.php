<?php

$form = \App\Tools\Form::getInstance();
$gallery = \App\Tools\Gallery::getInstance();
$user = \App\Tools\User::getInstance();

if (!isset($route['key']))
    \App\Tools\App::redirect(404);
if (empty($route['key']))
    \App\Tools\App::redirect(404);


$reset = $user->resetPassword($route['key']);

if (!$reset)
    \App\Tools\App::redirect(404);
?>

<div class="form-start">
    <p>We sent you a new mail with a temporary password, think to change it!</p>
</div>

