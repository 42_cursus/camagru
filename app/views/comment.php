<?php

$form = \App\Tools\Form::getInstance();
$gallery = \App\Tools\Gallery::getInstance();
$user = \App\Tools\User::getInstance();

if (!isset($route['key']))
    \App\Tools\App::redirect(404);
if (empty($route['key']) || !is_numeric($route['key']))
    \App\Tools\App::redirect(404);
if (!\App\Tools\Gallery::getInstance()->galleryIdExist($route['key']))
    \App\Tools\App::redirect(404);

$gal = $gallery->getGallery($route['key']);
$comments = $gallery->getComments($route['key']);
?>

<section id="page-header" class="clearfix">
    <div class="wrapper">
        <h1>Social Gallery</h1>
    </div>
</section>


<div id="main" class="wrapper clearfix">
    <section id="comment">
        <div style="text-align: center">
            <b style="display: block; color: #5ec79e"><?= $user->getOneById($gal['id_user'])['username'] ?></b>
            <img src="/public/img/captures/<?= $gal['name'] . ".png" ?>">
            <div>
                <?php foreach ($comments as $k => $v) { ?>
                <div class="comment-block">
                    <b style="color: #5ec79e;"><?= $user->getOneById($v['id_user'])['username'] ?></b> <i>( <?= $v['publish'] ?> )</i>
                    <div class="comment-content"><?= $v['content'] ?></div>
                </div>
                <?php } ?>
            </div>
        </div>
        <form method="POST" action="/forms/comment ">
            <label>Send your comment :</label>
            <input type="hidden" name="id" value="<?= $route['key'] ?>">
            <textarea name="content" style="width: 100%; height: 80px; resize: none;"></textarea>
            <?php $form->groupSubmit("submit", "Send"); ?>
        </form>
    </section>
</div>