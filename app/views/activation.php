<?php

$form = \App\Tools\Form::getInstance();
$gallery = \App\Tools\Gallery::getInstance();
$user = \App\Tools\User::getInstance();

if (!isset($route['key']))
    \App\Tools\App::redirect(404);
if (empty($route['key']))
    \App\Tools\App::redirect(404);


$activation = $user->activeAccount($route['key']);

if (!$activation)
    \App\Tools\App::redirect(404);
else
    \App\Tools\Mail::send($activation["email"], "Account activated", "Your account is now activated!");
?>

<div class="form-start">
    <p>Congratulations, your account is now activated!</p>
    <ul>
        <li>Username : <?= $activation["username"] ?></li>
        <li>Email : <?= $activation["email"] ?></li>
    </ul>

    <p>You can log in <a href="/auth">here</a></p>
</div>
