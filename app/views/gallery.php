<?php

$form = \App\Tools\Form::getInstance();
$gallery = \App\Tools\Gallery::getInstance();
$user = \App\Tools\User::getInstance();

?>

<section id="page-header" class="clearfix">

    <div class="wrapper">
        <h1>Social Gallery</h1>
    </div>
</section>


<div id="main" class="wrapper clearfix">
    <section id="gallery">
        <?php
        $g = $gallery->getAllGallery();
        if (count($g) == 0)
            echo "No captures";
        else
            foreach ($gallery->getAllGalleryLimits(0, 6) as $k => $v) {
        ?>
                <div class="gallery-item-capture">
                    <div class="gallery">
                        <div class="desc" style="color: #5ec79e"><?= $user->getOneById($v['id_user'])['username']?></div>
                        <a target="_blank" href="/public/img/captures/<?= $v['name'] . ".png" ?>">
                            <img src="/public/img/captures/<?= $v['name'] . ".png" ?>" alt="" width="300" height="200">
                        </a>
                        <div class="desc"><span id='<?= $v['id'] ?>-like'><?= $v['nb_like']?></span> <button onclick="setLikeGallery(<?= $v['id'] ?>)" class="thumb"></button>
                            <a href="/comment/<?= $v['id'] ?>" class="delete-gallery" style="text-decoration: none; color: black;float: right"><?= $v['nb_comments'] . " "?> <span class="chat" style="display: inline-block"></span></a>
                        </div>
                    </div>
                </div>
            <?php } ?>
    </section>
</div>
<script src="/public/js/infinite.js"></script>