<?php

namespace App\Tools;


class Mail {
    public static function send($to, $subject, $message) {
        $from = "adelhom@student.42.fr";
        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

        // Create email headers
        $headers .= 'From: '.$from."\r\n".
            'Reply-To: '.$from."\r\n" .
            'X-Mailer: PHP/' . phpversion();
        $message = '<html><body>' . $message . '</body></html>';
        return mail($to,$subject,$message, $headers);
    }
}