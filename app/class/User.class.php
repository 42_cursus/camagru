<?php

namespace App\Tools;

class User {
    private static $_instance;

    private $_db;

    public  function __construct() {
        $this->_db = Database::getInstance();
    }

    /*******************************************************************************************************************
     *                                                 SINGLETON
     ******************************************************************************************************************/

    public static function getInstance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new User();
        }
        return self::$_instance;
    }
    /*******************************************************************************************************************
     *                                          USER FORGOT PASSWORD
     ******************************************************************************************************************/

    public function resetPassword($key) {
        $user = $this->getOneByForgot($key);

        if (!$user)
            return false;
        $tmp_password = bin2hex(random_bytes(6));
        $tmp_hash = App::hash($tmp_password . REGISTER_SALT_SECRET);
        $this->_db->rawQuery("UPDATE users_template SET forgot = ?, password = ? WHERE id = ?",[null, $tmp_hash, $user["id"]]);
        Mail::send($user['email'], "Your temporary password", "This is your temporary password <b>".$tmp_password."</b>. Think to change it quickly!");
        return $user;
    }

    public function sendForgot($email) {
        $user = $this->_db->rawQuery("SELECT * FROM users_template WHERE email = ?", [$email], false);

        if (!$user)
            return "This user email doesn't exist";
        if ($user["activation"] !== null)
            return "You need to active your account before..";
        $forgot = bin2hex(random_bytes(22));
        $this->_db->rawQuery("UPDATE users_template SET forgot = ? WHERE id = ?",[$forgot, $user["id"]]);
        Mail::send($user["email"], "Password reset", "You can reset your password <a href='" . BASE_URL . "reset/" . $forgot . "'>HERE</a><br>Or raw URL : " . BASE_URL . "forgot/" . $forgot);
        return true;
    }

    /*******************************************************************************************************************
     *                                              USER REGISTER
     ******************************************************************************************************************/

    public function canRegister($posts) {
        $valide = array();
        $valide[] = $this->valideUsername($posts['username']);
        $valide[] = $this->valideEmail($posts['email']);
        $valide[] = $this->validePassword($posts['password1'], $posts['password2']);
        foreach ($valide as $e)
            if ($e !== true)
                return ($e);
        return (true);
    }

    public function register($posts) {
        $password = App::hash($posts['password1'] . REGISTER_SALT_SECRET);
        $activation = bin2hex(random_bytes(22));
        $datas = [$posts['username'], $posts['email'], $password, $activation];
        $this->_db->rawQuery("INSERT INTO users_template SET username = ?, email = ?, password = ?, activation = ?", $datas);
        Mail::send($posts["email"], "Active your account!", "You need to <a href='" . BASE_URL . "activation/" .$activation . "'>active your account</a><br> Raw url : ". BASE_URL . "activation/" .$activation);
    }

    /*******************************************************************************************************************
     *                                          USER AUTHENTIFICATION
     ******************************************************************************************************************/

    public function auth($usernameOrMail, $password) {
        $type = false;
        if ($this->usernameExist($usernameOrMail))
            $type = "username";
        else if ($this->emailExist($usernameOrMail))
            $type = "email";
        else
            return ("Incorrect pair to log in");
        $user = $this->_db->rawQuery("SELECT * FROM users_template WHERE $type = ?", [$usernameOrMail], false);
        if ($user["activation"] !== null)
            return ("You need to active your account !");
        if (hash_equals($user["password"], App::hash($password . REGISTER_SALT_SECRET))) {
            $cookie = $user['id'] . ":" . App::hash($user['id'] . COOKIE_SALT_SECRET);
            setcookie('session', $cookie, 2147483647, '/');
            $_SESSION['id'] = $user['id'];
            $_SESSION['level'] = $user['level'];
            return (true);
        }
        return ("Incorrect pair to log in");
    }

    public function authId($id) {
        if (!($user = $this->getOneById($id)))
            return (false);
        $_SESSION['id'] = $id;
        $_SESSION['level'] = $user['level'];
        return (true);

    }

    public function activeAccount($key) {
        $user = $this->getOneByActivation($key);

        if (!$user)
            return false;

        $this->_db->rawQuery("UPDATE users_template SET activation = ? WHERE id = ?",[null, $user["id"]]);
        return $user;
    }

    /*******************************************************************************************************************
     *                                            USER GET DATA
     ******************************************************************************************************************/

    public function getOneById($id) {
        return ($this->_db->rawQuery("SELECT * FROM users_template WHERE id = ?", [$id], false));
    }

    public function getAll() {
        return ($this->_db->rawQuery("SELECT * FROM users_template", [], true));
    }

    public function getOneByActivation($key) {
        return ($this->_db->rawQuery("SELECT * FROM users_template WHERE activation = ?", [$key], false));
    }

    public function getOneByForgot($key) {
        return ($this->_db->rawQuery("SELECT * FROM users_template WHERE forgot = ?", [$key], false));
    }

    /*******************************************************************************************************************
     *                                            USER SET DATA
     ******************************************************************************************************************/

    public function setPassword($new) {
        $id = $_SESSION['id'];
        $this->_db->rawQuery("UPDATE users_template SET password = ? WHERE id = ?",[App::hash($new . REGISTER_SALT_SECRET), $id]);
    }

    public function setUsername($new) {
        $id = $_SESSION['id'];
        $this->_db->rawQuery("UPDATE users_template SET username = ? WHERE id = ?",[$new, $id]);
    }

    public function setEmail($new) {
        $id = $_SESSION['id'];
        $this->_db->rawQuery("UPDATE users_template SET email = ? WHERE id = ?",[$new, $id]);
    }

    public function setNotify($b) {
        $id = $_SESSION['id'];
        $this->_db->rawQuery("UPDATE users_template SET notify = ? WHERE id = ?",[$b, $id]);
    }

    /*******************************************************************************************************************
     *                                            USER VALIDE DATA
     ******************************************************************************************************************/

    public function validePassword($password1, $password2=null) {
        if(empty($password1)) return "Empty password";
        if (!is_null($password2)) {
            if (empty($password1)) return "Empty password confirmation";
            if ($password1 != $password2) return "Not same passwords";
        }
        if(!in_array(strlen($password1), range(4, 32))) return "Password must contain between 4 & 32 characters";
        $oneUpper = (bool) preg_match('/[A-Z]/', $password1);
        if (!$oneUpper) return "Password must contain one upper character at least";
        return (true);
    }

    public function valideEmail($email) {
        if(empty($email)) return "Empty email";
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) return "Invalide email";
        if($this->emailExist($email)) return "Email already used";

        return (true);
    }

    public function valideUsername($username) {
        if(empty($username)) return "Empty username";
        if(!in_array(strlen($username), range(4, 18))) return "Username must contain between 4 & 18 characters";
        if(!preg_match("/^[a-zA-Z0-9-]+$/", $username)) return "Invalide username";
        if($this->usernameExist($username)) return "Username already used";

        return (true);
    }


    /*******************************************************************************************************************
     *                                                 USER STATUS
     ******************************************************************************************************************/

    public function isConnected() {
        if(!isset($_SESSION['id'])) {
            $cookie = isset($_COOKIE['session']) ? $_COOKIE['session'] : false;
            if ($cookie !== false) {
                if (!(list ($id, $hash) = explode(':', $cookie)))
                    return (false);
                if (!hash_equals(App::hash($id . COOKIE_SALT_SECRET), $hash))
                    return (false);
                return ($this->authId($id));
            }
            else
                return (false);
        }
        else
            return (true);
    }

    public function logout() {
        session_destroy();
        unset($_COOKIE['session']);
        setcookie('session', null, -1, '/');
        App::redirect('/');
    }

    /*******************************************************************************************************************
     *                                               USER DATA EXIST
     ******************************************************************************************************************/

    public function usernameExist($username) {
        $request = $this->_db->rawQuery("SELECT * FROM users_template WHERE username = ?", [$username], false);
        return (!is_array($request)) ? false : true;
    }

    public function emailExist($email) {
        $request = $this->_db->rawQuery("SELECT * FROM users_template WHERE email = ?", [$email], false);
        return (!is_array($request)) ? false : true;
    }

    public function idExist($id) {
        $request = $this->_db->rawQuery("SELECT * FROM users_template WHERE id = ?", [$id], false);
        return (!is_array($request)) ? false : true;
    }
    
    /*******************************************************************************************************************
     *                                               USER HELPERS
     ******************************************************************************************************************/

    public function alert($types=array()) {
        foreach ($types as $type) {
            $datas = array();
            if (!isset($_SESSION[$type]) || empty($_SESSION[$type]))
                continue ;
            switch ($type) {
                case "error":
                    $datas = ["Error !", $_SESSION[$type], "error"];
                    break;
                case "success":
                    $datas = ["Success !", $_SESSION[$type], "success"];
                    break;
                default:
                    break ;
            }
            echo '<div class="alert ' . $datas[2] . '" onclick="this.style.display=\'none\';">
                    <strong>' . $datas[0] . '</strong> ' . $datas[1] . '
                  </div>';
            $_SESSION[$type] = "";
        }
    }


}