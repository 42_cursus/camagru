<?php

namespace App\Tools;

class Database {
    private static $_instance;

    private $_db;
    private $_stmt;
    private $_error;
    private $_binds;

    public function __construct($init=false) {
        $options = array(
            \PDO::ATTR_PERSISTENT    => true,
            \PDO::ATTR_ERRMODE       => \PDO::ERRMODE_EXCEPTION
        );
        try{
            $this->_db = new \PDO(($init) ? DB_DSN_INIT : DB_DSN, DB_USER, DB_PASS, $options);
            if ($init) {
                $this->_db->query("CREATE DATABASE IF NOT EXISTS " . DB_NAME);
                $this->_db->exec('use ' . DB_NAME);
            }
        }
        catch(\PDOException $e){
            $this->_error = $e->getMessage();
        }
        $this->_binds = array();
        $this->_stmt = new \PDOStatement();
    }

    /*******************************************************************************************************************
     *                                                 SINGLETON
     ******************************************************************************************************************/

    public static function getInstance($init=false) {
        if (is_null(self::$_instance))
            self::$_instance = new Database($init);
        return self::$_instance;
    }

    /*******************************************************************************************************************
     *                                                 INIT DB
     ******************************************************************************************************************/

    public function runSQLFile($file) {
        $sql = file_get_contents($file);
        $qr = $this->_db->exec($sql);
        return $sql;
    }

    /*******************************************************************************************************************
     *                                           PDO FUNCTIONS
     ******************************************************************************************************************/

    public function query($query){
        $this->_stmt = $this->_db->prepare($query);
    }

    public function bind($param, $value, $type = null) {
        if (is_null($type)) {
            switch (true) {
                case is_int($value):
                    $type = \PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = \PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = \PDO::PARAM_NULL;
                    break;
                default:
                    $type = \PDO::PARAM_STR;
            }
        }
        $this->_stmt->bindValue($param, $value, $type);
    }

    public function execute() {
        return $this->_stmt->execute();
    }

    public function resultset() {
        $this->execute();
        return $this->_stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function single() {
        $this->execute();
        return $this->_stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function rowCount() {
        return $this->_stmt->rowCount();
    }

    public function lastInsertId() {
        return $this->_db->lastInsertId();
    }

    public function beginTransaction() {
        return $this->_db->beginTransaction();
    }

    public function endTransaction() {
        return $this->_db->commit();
    }

    public function cancelTransaction() {
        return $this->_db->rollBack();
    }

    public function debugDumpParams() {
        return $this->_stmt->debugDumpParams();
    }

    public function setBinds($binds) {
        foreach ($binds as $k => $v)
            $this->bind((int)($k + 1), $v);
    }

    public function clearBinds() {
        $this->_binds = array();
    }

    /*******************************************************************************************************************
     *                                          HELPERS QUERIES
     ******************************************************************************************************************/
    
    public function rawQuery($query, $binds=array(), $fetchAll=null) {
        $this->query($query);
        $this->setBinds($binds);
        if (!is_bool($fetchAll))
            $result = $this->execute();
        else
            $result = ($fetchAll === true) ? $this->resultset() : $this->single();
        $this->clearBinds();
        return ($result);
    }
}
?>