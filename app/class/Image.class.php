<?php

namespace App\Tools;


class Image {
    private $_datas;

    public function __construct($datas) {
        $this->_datas = $datas;
    }

    public function create() {
        if ($this->_datas['s'] < 100 || $this->_datas['s'] > 1000)
            return (false);
        if ($this->_datas['x'] < -400 || $this->_datas['x'] > 800 || $this->_datas['y'] < -400 || $this->_datas['y'] > 800)
            return (false);
        if (!($this->_datas['source'] = $this->base64ToImg($this->_datas['source'])))
            return (false);
        if (!($this->_datas['sourceIcon'] = $this->base64ToImg($this->_datas['sourceIcon'])))
            return (false);
        $name = App::randomString(12);

        if ($this->mergeSources($name))
            $this->addToDatabase($_SESSION['id'], $name);
        else
            return (false);
        return (true);
    }

    private function mergeSources($name){
        if (imagecopyresized(
            $this->_datas['source'],
            $this->_datas['sourceIcon'],
            0,
            0,
            0,
            0,
            400,
            400,
            400,
            400
        ))
        if (imagepng($this->_datas['source'], "public/img/captures/$name.png", 0))
            return (true);
        return (false);
    }
    
    private function base64ToImg($base64){
        list($type, $base64) = explode(';', $base64);
        list(, $base64)      = explode(',', $base64);
        $tmp = @imagecreatefromstring(base64_decode($base64));
        if ($tmp === false)
            return (false);
        imagealphablending($tmp, true);
        imagesavealpha($tmp, true);
        return ($tmp);
    }

    function addToDatabase($id, $name) {
        Database::getInstance()->rawQuery("INSERT INTO gallery_template (id_user, name, publish) VALUES (?, ?, ?)", [$id, $name, date('Y-m-d H:i:s')]);
    }

}