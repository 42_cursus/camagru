<?php

namespace App\Tools;

class Form {
    private static $_instance;
    private $_posts;
    private $_expected;

    public function __construct() {
        $this->_posts = array();
        $this->_expected = array();
    }

    /*******************************************************************************************************************
     *                                                 SINGLETON
     ******************************************************************************************************************/

    public static function getInstance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new Form();
        }
        return self::$_instance;
    }

    /*******************************************************************************************************************
     *                                            GET & SET & DEL $_posts
     ******************************************************************************************************************/

    public function getPosts() {
        return ($this->_posts);
    }

    public function setPosts($posts) {
        $this->_posts = $posts;
    }

    public function getOnePosts($key) {
        return ($this->_posts[$key]);
    }

    public function delOnePosts($key) {
        unset($this->_posts[$key]);
    }

    /*******************************************************************************************************************
     *                                         GET & SET & DEL $_expected
     ******************************************************************************************************************/

    public function getExpected() {
        return ($this->_expected);
    }

    public function setExpected($array = array()) {
        $this->_expected = $array;
    }

    public function delOneExpected($value) {
        if (($key = array_search($value, $this->_expected)) !== false)
            unset($this->_expected[$key]);
    }

    /*******************************************************************************************************************
     *                                           CHECK FORMS DATAS
     ******************************************************************************************************************/

    public function isValide() {
        if (count($this->_expected) == 0 || count($this->_posts) == 0)
            return (false);
        if (count($this->_posts) != count($this->_expected))
            return (false);
        foreach ($this->_expected as $e) {
            if (!isset($this->_posts[$e]))
                return (false);
            if (is_null($this->_posts[$e]))
                return (false);
        }
        return (true);
    }

    /*******************************************************************************************************************
     *                                                 HTML TAGS
     ******************************************************************************************************************/

    public function label($name, $text, $icon=null) {
        $label = '<label for="'.$name .'"';
        $label .= ($icon !== null) ? 'data-icon="'. $icon .'">'.$text.'</label>' : '>'.$text.'</label>';
        echo $label;
    }

    public function input($name, $id, $type, $placeholder="", $required=true) {
        if (empty($id)) $id = $name;
        $required = ($required) ? "required" : "";
        $input = '<input id="'.$id.'" name="'. $name.'" '.$required.' type="'.$type.'" placeholder="'.$placeholder.'"/>';
        echo $input;
    }

    public function range($name, $id, $range, $step, $default) {
        echo "<input name='$name' id='$id' type='range' min='$range[0]' max='$range[1]' value='$default' step='$step'/>";
    }

    public function submit($name, $value, $disabled=false) {
        $sub = '<input id="'.$name.'" name="'. $name.'" class="button" type="submit" value="'.$value.'"';
        $sub .= ($disabled) ? ' disabled />' : '/>';
        echo $sub;
    }

    public function radioFilter() {

    }

    /*******************************************************************************************************************
     *                                             HTML GROUP TAGS
     ******************************************************************************************************************/

    public function groupSubmit($name, $value) {
        echo '<div class="submit-group">';
            $this->submit($name, $value);
        echo '</div>';
    }

    public function groupInput($name, $id, $type, $label=null, $icon=null, $placeholder="", $required=true) {
        echo '<div class="input-group">';
                $this->label($id, $label, $icon);
                $this->input($name, $id, $type, $placeholder, $required);
        echo '</div>';
    }

    public function groupRange($name, $id, $label, $range, $step=1, $default=0) {
        $this->label($id, $label);
        $this->range($name, $id, $range, $step, $default);
    }

    /*******************************************************************************************************************
     *                                             ALL FILTERS RADIO
     ******************************************************************************************************************/

    public function allIcons() {
        $icons = array_values(array_diff(scandir($_SERVER["DOCUMENT_ROOT"] . "/public/img/icons"), array('..', '.')));
        echo "<ul style='list-style: none; text-align: center'>";
        foreach ($icons as $icon) {
            echo "<li style='display: inline-block; margin-left: 15px; margin-right: 15px'>";
            $path = $_SERVER["DOCUMENT_ROOT"] . "/public/img/filters/" . $icon;
            $id = pathinfo($path, PATHINFO_FILENAME);
            $this->groupInput("icons", $id, "radio", "<img width='100' height='100' src='/public/img/icons/$icon'>");
            echo "</li>";
        }
        echo "</ul>";
    }
}
?>