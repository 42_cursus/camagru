<?php

namespace App\Tools;

class Router
{
    private static $_instance;

    private $_routes;
    private $_currentRoute;
    private $_currentPath;

    public function __construct() {
        $this->_routes = array();
    }

    /*******************************************************************************************************************
     *                                                 SINGLETON
     ******************************************************************************************************************/

    public static function getInstance() {
        if (is_null(self::$_instance))
            self::$_instance = new Router();
        return self::$_instance;
    }

    /*******************************************************************************************************************
     *                                              ROUTES METHODES
     ******************************************************************************************************************/

    public function getRoutes() {
        return ($this->_routes);
    }

    public function addRoute($route, $path, $type, $level, $exact_level) {
        if (!empty($path))
            $path .= ".php";
        $this->_routes[$route] = compact('route', 'path', 'type', 'level', 'exact_level');
    }

    private function buildRoute() {
        $routeKey = (!isset($_GET[1])) ? "/" : "/" . $_GET[1];
        if (substr($routeKey, 0, strlen("/comment")) === "/comment") {
            $params = explode("/", $routeKey);
            if (sizeof($params) != 3)
                $routeKey = "/404";
            else {
                $routeKey = "/comment";
                $key = $params[2];
            }
        }
        else if (substr($routeKey, 0, strlen("/activation")) === "/activation") {
            $params = explode("/", $routeKey);
            if (sizeof($params) != 3)
                $routeKey = "/404";
            else {
                $routeKey = "/activation";
                $key = $params[2];
            }
        }
        else if (substr($routeKey, 0, strlen("/reset")) === "/reset") {
            $params = explode("/", $routeKey);
            if (sizeof($params) != 3)
                $routeKey = "/404";
            else {
                $routeKey = "/reset";
                $key = $params[2];
            }
        }
        else if (!array_key_exists($routeKey, $this->_routes))
            $routeKey = "/404";
        $this->_currentRoute = $this->_routes[$routeKey];
        $this->_currentPath = $this->_currentRoute['path'];
        if (isset($key))
            $this->_currentRoute['key'] = $key;
    }

    public function getCurrentRoute() {
        $this->buildRoute();
        return ($this->_currentRoute);
    }
    /*******************************************************************************************************************
     *                                              PATH METHODES
     ******************************************************************************************************************/

    private function checkLevel() {
        $connected = User::getInstance()->isConnected();
        switch ($this->_currentRoute['type']) {
            case START_LAYOUT:      if ($connected === true) App::redirect("/");       break;
            case DEFAULT_LAYOUT:    if ($connected === false) App::redirect("/auth");   break;
        }
    }

    public function getPath() {
        $this->checkLevel();
        switch ($this->_currentRoute['type']) {
            case START_LAYOUT:      $this->_currentPath =   LAYOUTS_DIR     .       "start.php";            break;
            case DEFAULT_LAYOUT:    $this->_currentPath =   LAYOUTS_DIR     .       "default.php";          break;
            case ERROR_LAYOUT:      $this->_currentPath =   VIEWS_DIR       .       $this->_currentPath;    break;
            case HELPERS:           $this->_currentPath =   HELPERS_DIR     .       $this->_currentPath;    break;
        }
        return ($this->_currentPath);
    }
}
?>