<?php

namespace App\Tools;

class Gallery
{
    private static $_instance;

    private $_db;

    public function __construct()
    {
        $this->_db = Database::getInstance();
    }

    /*******************************************************************************************************************
     *                                                 SINGLETON
     ******************************************************************************************************************/

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new Gallery();
        }
        return self::$_instance;
    }

    public function getUserGallery($id_user) {
        $result = $this->_db->rawQuery("SELECT * FROM gallery_template WHERE id_user = ? ORDER BY publish DESC", [$id_user], true);
        foreach ($result as $k => $v) {
            $result[$k]['nb_like'] = $this->_db->rawQuery("SELECT count(*) as r FROM gallery_like_template WHERE id_gallery = ?", [$v['id']], false)['r'];
            $result[$k]['nb_comments'] = $this->_db->rawQuery("SELECT count(*) as r FROM gallery_comment_template WHERE id_gallery = ?", [$v['id']], false)['r'];
        }
        return $result;
    }

    public function getGallery($id) {
        return ($this->_db->rawQuery("SELECT * FROM gallery_template WHERE id = ?", [$id], false));
    }

    public function deleteGallery($id) {
        $name =  $this->_db->rawQuery("SELECT name FROM gallery_template WHERE id = ?", [$id], false)['name'];
        unlink("public/img/captures/$name.png");
        $this->_db->rawQuery("DELETE FROM gallery_template WHERE id = ?", [$id]);
        $this->_db->rawQuery("DELETE FROM gallery_like_template WHERE id_gallery = ?", [$id]);
        $this->_db->rawQuery("DELETE FROM gallery_comment_template WHERE id_gallery = ?", [$id]);
    }

    public function getAllGallery() {
        return ($this->_db->rawQuery("SELECT * FROM gallery_template", [], true));
    }

    public function getAllGalleryLimits($min, $max) {
        $result = $this->_db->rawQuery("SELECT * FROM gallery_template  ORDER BY publish DESC LIMIT $min, $max", [], true);
        foreach ($result as $k => $v) {
            $result[$k]['nb_like'] = $this->_db->rawQuery("SELECT count(*) as r FROM gallery_like_template WHERE id_gallery = ?", [$v['id']], false)['r'];
            $result[$k]['nb_comments'] = $this->_db->rawQuery("SELECT count(*) as r FROM gallery_comment_template WHERE id_gallery = ?", [$v['id']], false)['r'];
        }
        return ($result);
    }

    function likeExist($id) {
        $request = $this->_db->rawQuery("SELECT * FROM gallery_like_template WHERE id_user = ? AND id_gallery = ?", [$_SESSION['id'], $id], false);
        return (!is_array($request)) ? false : true;
    }

    function setLike($id) {
        $gallery = $this->getGallery($id);
        Database::getInstance()->rawQuery("INSERT INTO gallery_like_template (id_user, id_gallery) VALUES (?, ?)", [$_SESSION['id'], $id]);
        if ($_SESSION['id'] != $gallery['id_user']) {
            $target = User::getInstance()->getOneById($gallery['id_user']);
            if ($target['notify'] == 1) {
                $user = User::getInstance()->getOneById($_SESSION['id']);
                Mail::send($target['email'], "New like!","New like on <a href='" . BASE_URL . "comment/" . $gallery['id'] . "'>this post</a> by " . $user['username']);
            }
        }
    }

    function setComment($id, $content) {
        $gallery = $this->getGallery($id);
        Database::getInstance()->rawQuery("INSERT INTO gallery_comment_template (id_gallery, id_user, content, publish) VALUES (?, ?, ?, ?)", [$id, $_SESSION['id'], $content, date('Y-m-d H:i:s')]);
        if ($_SESSION['id'] != $gallery['id_user']) {
            $target = User::getInstance()->getOneById($gallery['id_user']);
            if ($target['notify'] == 1) {
                $user = User::getInstance()->getOneById($_SESSION['id']);
                Mail::send($target['email'], "New comment!","New comment on <a href='" . BASE_URL . "comment/" . $gallery['id'] . "'>your post</a> by " . $user['username']);
            }
        }
    }

    public function galleryIdExist($id) {
        $request = $this->_db->rawQuery("SELECT * FROM gallery_template WHERE id = ?", [$id], false);
        return (!is_array($request)) ? false : true;
    }

    public function getComments($id) {
        return ($this->_db->rawQuery("SELECT * FROM gallery_comment_template WHERE id_gallery = ?", [$id], true));
    }
}