<?php

namespace App\Tools;

require_once "autoloader.php";

class App {
    private static $_instance;

    private $_form;
    
    public function __construct() {
        $this->_form = Form::getInstance();
    }

    /*******************************************************************************************************************
     *                                                 SINGLETON
     ******************************************************************************************************************/

    public static function getInstance() {
        if (is_null(self::$_instance))
            self::$_instance = new App();
        return self::$_instance;
    }

    /*******************************************************************************************************************
     *                                                 HELPERS
     ******************************************************************************************************************/

    public static function isXMLHttpRequest() {
        if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || $_SERVER['HTTP_X_REQUESTED_WITH'] !== 'XMLHttpRequest')
            self::redirect(404);
    }

    public static function redirect($route) {
        switch($route) {
            case 404:   header('Location: /404');       break;
            case 403:   header('Location: /403');       break;
            default:    header('Location: '. $route);   break;
        }
        exit();
    }

    public static function redirectSuccessMessage($message, $route) {
        $_SESSION['success'] = $message;
        self::redirect($route);
    }

    public static function redirectErrorMessage($message, $route) {
        $_SESSION['error'] = $message;
        self::redirect($route);
    }

    public static function debugDumpVar($var) {
        if (DEBUG)
            echo "<pre style='background: black; color:white'>" . var_export($var, true) . "</pre>";
    }

    public static function hash($string) {
        return (hash('whirlpool', $string));
    }

    public static function randomString($length) {
        $r ='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle(str_repeat($r, ceil($length/strlen($r)))), 1, $length);
    }

}
?>