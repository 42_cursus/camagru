/***********************************************************************************************************************
 *                                                    GALLERY
 **********************************************************************************************************************/

function userGallery() {
    ajaxData("/usergallery", null, function (e) {
        gallery = document.getElementById("usergallery");
        gallery.innerHTML = "";
        if (e.length == 0)
            gallery.innerHTML = "No captures";
        e.forEach(function (item) {
            gallery.innerHTML +=
                '<div class="gallery-item-capture">'
                +'<div class="gallery">'
                + '<a target="_blank" href="/public/img/captures/'+ item.name + '.png">'
                +'<img src="/public/img/captures/'+ item.name + '.png" alt="" width="300" height="200">'
                +'</a>'
                +'<div class="desc">'
                    + item.nb_like + ' <span style="display: inline-block" class="thumb"></span>'
                    + item.nb_comments + ' <span style="display: inline-block" class="chat"></span>'
                    +'<span onclick="deleteUserGallery(this)" id="' + item.id + '" class="delete-gallery" style="float: right">X</span>'
                +'</div>'
                +'</div>'
                +'</div>';
        });
    });
}

function deleteUserGallery(el) {
    id = el.getAttribute("id");
    item = el.parentNode.parentNode.parentNode;
    ajaxData("/deleteusergallery", {'id': id}, function (e) {
        if (e === "success") {
            item.remove();
            userGallery();
            showAlert("success", "Image removed!");
        }
    });
}

function setLikeGallery(id) {
    ajaxData("/like", {'id': id}, function (e) {
        if (e === "success") {
            document.getElementById(id+'-like').innerText = parseInt(document.getElementById(id+'-like').innerText) + 1;
            showAlert("success", "Thx for like!");
        }
        else
            showAlert("error", e);
    });
}
/***********************************************************************************************************************
 *                                                    HELPERS
 **********************************************************************************************************************/

function showAlert(type, msg) {
    strong = (type === "success") ? "Success!" : "Error!";
    alert = '<div class="alert '+ type + '" onclick="this.style.display=\'none\';">'
        + '<strong>' + strong + '</strong> ' + msg
        + '</div>';
    document.getElementById("alertDatas").innerHTML += alert;
    window.setTimeout( function () {
        document.getElementById("alertDatas").firstElementChild.remove();
    }, 4000);
}


function ajaxData(url, data, callback) {
    addr = location.href.substring(0, location.href.indexOf('/', 8));
    var xhr = new XMLHttpRequest();
    addr += url;
    xhr.open("POST", addr, true);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            callback(JSON.parse(xhr.responseText));
        }
    };
    var datas = JSON.stringify(data);
    xhr.send(datas);
}