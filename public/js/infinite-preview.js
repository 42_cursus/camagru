var min = 6;
var max = 3;

function loadMore() {
    ajaxData("/allgallery", {'min': min, 'max': max}, function (e) {
        e.forEach(function (item) {
            console.log('ok');
            gallery.innerHTML +=
                '<div class="gallery-item-capture">'
                +'<div class="gallery">'
                +'<div class="desc" style="color: #5ec79e">'+ item.username + '</div>'
                +'<a target="_blank" href="/public/img/captures/' + item.name + '.png">'
                +'<img src="/public/img/captures/' + item.name + '.png" alt="" width="300" height="200">'
                +'</a>'
                +'</div>'
                +'</div>'
        });
    });
}

var gallery = document.getElementById('gallery');
window.onscroll = function(ev) {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {

        min += 3;
        loadMore();

    }
};

