/***********************************************************************************************************************
 *                                                   GLOBAL VAR
 **********************************************************************************************************************/

// webcam video
var webcam = document.getElementById("webcam");
// webcam canvas
var source = document.createElement("canvas");
// canvas icon
var icon = document.getElementById("icon");
var iconCtx = icon.getContext("2d");
var iconImg = new Image();
// radios icons
var radios = document.getElementsByName("icons");
// capture datas
var capture = {
    source: "",
    sourceIcon: "",
    id: "",
    x: "",
    y: "",
    s: ""
};

/***********************************************************************************************************************
 *                                                     WEBCAM
 **********************************************************************************************************************/

function webcamSuccess(stream) {
    webcam.srcObject  = stream;
    document.getElementById("content").style.display = "block";
    webcam.style.display = "inline-block";
}

function webcamError(error) {
    webcam = document.getElementById("upload-render");
    document.getElementById("content").style.display = "block";
    webcam.style.display = "inline-block";
    icon.addEventListener('click', function () {
        document.getElementById("upload").click();
    });
    document.getElementById("upload").onchange = function () {
        source.getContext('2d').clearRect(0, 0, source.width, source.height);
        var reader = new FileReader();
        reader.onload = function (e) {
            document.getElementById("upload-render").src = e.target.result;
        };
        reader.readAsDataURL(this.files[0]);
    };
}

function webcamSource() {
    source.width = 400;
    source.height = 400;
}

function webcamInit() {
    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
    constraints = {
        audio: false,
        video: {
            mandatory: {
                maxWidth: 400,
                maxHeight: 400
            }
        }
    };
    navigator.mediaDevices.getUserMedia(constraints).then(webcamSuccess).catch(webcamError);
}


/***********************************************************************************************************************
 *                                                      ICON
 **********************************************************************************************************************/

function radioChecked() {
    previous = null;
    radios.forEach(function(radio) {
        radio.onclick = function() {
            document.getElementById("capture").disabled = false;
            (previous) ? previous.parentElement.style.opacity = 1 : null;
            previous = this;
            this.parentElement.style.opacity = 0.5;
            setCaptureId(this.id);
            iconImg.src = "/public/img/icons/" + this.id + ".png";
        };
    });
}

function drawIcon(x, y, s) {
    setCapturePosition(x, y, s);
    iconCtx.clearRect(0, 0, icon.width, icon.height);
    iconCtx.save();
    iconCtx.translate(x, y);
    iconCtx.drawImage(iconImg, -s / 2, -s / 2, s, s);
    iconCtx.restore();
}

function rangeEvent() {
    x = document.getElementById("iconX");
    y = document.getElementById("iconY");
    s = document.getElementById("iconS");
    x.oninput = function() {drawIcon(x.value, y.value, 100*(s.value/10))};
    y.oninput = function() {drawIcon(x.value, y.value, 100*(s.value/10))};
    s.oninput = function() {drawIcon(x.value, y.value, 100*(s.value/10))};

    drawIcon(x.value, y.value, 100*(s.value/10));
}

/***********************************************************************************************************************
 *                                                  CAPTURE
 **********************************************************************************************************************/

function setCapturePosition(x, y, s) {
    capture.x = x;
    capture.y = y;
    capture.s = s;
}

function setCaptureId(id) {
    capture.id = id;
}

function setCaptureSource(sendcallback) {
    try {
        source.getContext('2d').drawImage(webcam, 0, 0, source.width, source.height);
        capture.source = source.toDataURL('image/png');
        capture.sourceIcon = icon.toDataURL('image/png');
        sendcallback();
    }
    catch (e) {
        showAlert("error", "Send an image please!");
    }
}

function sendCapture() {
    setCaptureSource(function () {
        ajaxData("/capture", capture, function (e) {
            if (e === "success") {
                userGallery();
                showAlert("success", "Capture added !");
            }
        });
    });
}


/***********************************************************************************************************************
 *                                                   EXECUTION
 **********************************************************************************************************************/

webcamInit();
webcamSource();
radioChecked();
iconImg.onload = function() {
    rangeEvent();
};

document.getElementById("capture").addEventListener('click', sendCapture);